<?php

namespace App\Http\Controllers\modulos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $modulo='home';

    public function index(){

		return view('landing/'.$this->modulo.'/index',[
								'css' => [
                  'landing/'.$this->modulo.'/owl.carousel.min.css',
                  'landing/'.$this->modulo.'/owl.theme.default.min.css',
                  'landing/'.$this->modulo.'/owl.theme.green.min.css',
                  'landing/'.$this->modulo.'/estilos.css',
                  'landing/'.$this->modulo.'/carrusel.css'
                ],
								'jspre' => [
                  'landing/'.$this->modulo.'/owl.carousel.min.js',
                ],
                'js' => [
                  'landing/'.$this->modulo.'/script.js',
                ],
								'modulo' => $this->modulo,
                'ciudades' => require('utilidad/ciudades.php'),
						   ]);
	}
}



var sticky = $('#idFooter').offset();

$(window).scroll(function() {
  if (window.pageYOffset <= 1100) {
    $('.form-animate').removeClass("topForm");
    $('.btn-animate').removeClass("topForm");
    $('.btn-animate').addClass("btn");
    $('.btn-animate').addClass("btn-block");
    $('.btn-animate').addClass("buttonFormPrimary");
  } else {
    $('.form-animate').addClass("topForm");
    $('.btn-animate').addClass("topForm");
    $('.btn-animate').removeClass("btn");
    $('.btn-animate').removeClass("btn-block");
    $('.btn-animate').removeClass("buttonFormPrimary");
  }
});

// Smooth scrolling using jQuery easing
$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: ((target.offset() && target.offset().top) ? target.offset().top : 0 != - 54)
      }, 1000, "easeInOutExpo");
      return false;
    }
  }
});


// Closes responsive menu when a scroll trigger link is clicked
$('.js-scroll-trigger').click(function() {
  $('.navbar-collapse').collapse('hide');
});


$(document).ready(function(){
  // Collapse Navbar
  var navbarCollapse = function() {
    if (($("#mainNav").offset() && $("#mainNav").offset().top) ? target.offset().top : 0 > 100) {
      $("#mainNav").addClass("navbar-shrink");
      $("#mainNav").addClass("shadow");

    } else {
      $("#mainNav").removeClass("navbar-shrink");
      $("#mainNav").removeClass("shadow");
    }
     if (($("#mainNav").offset() && $("#mainNav").offset().top) ? target.offset().top : 0 > 200 && (screen.width > 991)  ) {
      $(".ocultarForm").hide();
    }else{
      $(".ocultarForm").show();
    }
  };
    // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  envioFormulariosCallback();
});

 function getGETUrl(){
        // capturamos la url
        var loc = document.location.href;
        // si existe el interrogante
        if(loc.indexOf('?')>0)
        {
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            var GET = getString.split('&');
            var get = {};

            // recorremos todo el array de valores
            for(var i = 0, l = GET.length; i < l; i++){
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }
            return get;
        }else{
          return {};
        }
    }


function envioFormulariosCallback(){
  // para el envio Callback1 parametros (cola,url)
  const callback1= new Callback({"fijo":'4',"movil":'2'},'https://so04.kerberusipbx.com:625/api/v0.1/callback');
  callback1.setColaDefault("movil");
  // para el envio Callback2 parametros (nombre_cola,url,intentos,tiempoEspera,amd,troncal)
  const callback2= new Callback2({fijo:'Ecuador_C', movil:"MovilCL"},'https://so12.kerberusipbx.com:625/api/v0.1/callbackv2', 2, 25, false, 'Ecuador_New');
  callback2.setColaDefault("movil");
  // funcciones de utilidad como llo son lista negra, horario de atencion, tsorce .....
  const utilidad= new Utilidad();
  utilidad.cargarTsource('movil'); // funcion para cargar tsource

  // para envio formulario callback principal
  $("#formulario-principal").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) {
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#formulario-principal"),event);
      callback1.setExtra({tsource:window.location.href});
      // if(utilidad.validarListaNegra(callback1.getNumero())){
        callback1.enviar();
      // }
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });

  // para envio formulario callback Modal
  $("#form-contato").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) {
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#form-contato"),event);
      callback1.setExtra({tsource:window.location.href});
      // if(utilidad.validarListaNegra(callback1.getNumero())){
        callback1.enviar();
      // }
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });

  // para envio formulario callback Modal
  $("#formulario-modal1").submit(function(event){
    // if (utilidad.validarHorarioAtencion()) {
      // se envia el elemeto form y evento
      callback1.setVariablesPorFormulario($("#formulario-modal1"),event);
      callback1.setExtra({tsource:window.location.href});
      // if(utilidad.validarListaNegra(callback1.getNumero())){
        callback1.enviar();
      // }
      utilidad.gracias();
    // }else{
    //   utilidad.fueraHorario();
    // }
  });
}

function ocultarMovil(tipo=true){
    if(!tipo){
      $(".ocultarMovil").removeClass("d-none");
      $(".labelFijo").removeClass("label-default");
      $(".labelMovil").removeClass("label-active");
      $(".labelFijo").addClass("label-active");
      $(".labelMovil").addClass("label-default");
    }else{
      $(".ocultarMovil").addClass("d-none");
      $(".labelFijo").removeClass("label-active");
      $(".labelFijo").addClass("label-default");
      $(".labelMovil").removeClass("label-default");
      $(".labelMovil").addClass("label-active");
    }
}

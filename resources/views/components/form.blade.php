<div class="ClassMigrar text-center">
  Migra llamando a:

  <?php if (isset($btnClose)): ?>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true" style="font-size: 25px;">&times;</span>
    </button>
  <?php endif; ?>

</div>
<div class="Classlinea text-center">
  <h5 class="textLinea">Linea gratuita</h5>
  <a class="NumLlama" href="">0800 71996</a>
</div>
<div class="form-animate card text-center">
  <h5 class="textLinea">O nosotros te llamamos</h5>
  <div class="row">
    <div class="col-6 d-flex align-items-center justify-content-center">
      <h6 class="textElige">Elige tu opción</h6>
    </div>
    <div class=" col-6 row-flex">
      <div class="col-12 d-column" style="padding: 0px;">
        <input type="radio" id="tipo_movil<?=$ambito?>" name="tipo_movil<?=$ambito?>" style="display: none;" value="movil" onclick="ocultarMovil(true)"  >
        <label class="labelMovil label-active m-0" for="tipo_movil<?=$ambito?>">
          <img src="img/IconsClaro/cellPhone.png" alt="">
          Móvil
        </label>

        <input type="radio" id="tipo_llamada<?=$ambito?>" name="tipo_llamada<?=$ambito?>" style="display: none;" value="fijo" onclick="ocultarMovil(false)" required="required">
        <label class="labelFijo label-default m-0" for="tipo_llamada<?=$ambito?>">
          <img src="img/IconsClaro/digital-phone.png" alt="">
          Fijo
        </label>
        </div>
    </div>
  </div>
  <div class="row m-0 mb-2 ocultarMovil d-none">
    <div class="col-12">
      <select class="form-control" name="">
        @component('components.selectCities', ['ciudades' => $ciudades])
        @endcomponent
      </select>
    </div>
  </div>

  <div class="col-12 row m-0 mb-2">
    <div class="col-12">
      <input type="text" name="numero" class="inputFormSection2" placeholder="Ingresa tu @lang('general.numero')" required="required">
    </div>
  </div>

  <div class="form-check text-center">
    <input class="form-check-input"  type="checkbox" id="check_1" required="">
    <label class="form-check-label TextTerminos" id="check_label_1" for="check_1">
      <a class="TextTerminos" href="<?=url("/")?>/terminos" target="_black">Autorizo el tratamiento de mis datos personales</a>
    </label>
  </div>
</div>

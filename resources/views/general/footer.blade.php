<!-- Footer -->
<footer class="container-fluid">
  <div class="row background-grey">

    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
      <h4 class="titleCambiate">!Cámbiate ahora!</h4>
    </div>
    <div class="container col-lg-4 col-md-4 col-12">
      <div class="classMovilColumn row justify-content-center align-items-center">
        <span class="labelsFooter1">Tu nombre</span>
        <input
        class="inputForm"
        id="nombre"
        name="nombre"
        type="text"
        placeholder=""
        required="required"
        data-validation-required-message="Please enter your name.">
      </div>
    </div>
    <div class="container col-lg-4 col-md-4 col-12">
      <div class="classMovilColumn row justify-content-center align-items-center">
        <span class="labelsFooter1">Tu correo</span>
        <input
        class="inputForm"
        id="nombre"
        name="correo"
        type="email"
        placeholder=""
        required="required"
        data-validation-required-message="Please enter your name.">
      </div>
    </div>

    <div class="classMovilColumn q-pt-20 col-12 row-flex justify-content-center align-items-center">
      <div class="ClassMovilCheck form-check q-pr-20">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Acepto la política de privacidad</label>
      </div>

      <button type="button" class="classMovilbtnform btn btn-outline-light">Enviar</button>
    </div>
  </div>
  <div class="row background-black">
    <div class="col-12 justify-content-between">
      <div class="container-fluid footer2">
        <div class="row">
          <div class="classMovilfootes2 col-md-3 col-sm-12 d-flex align-items-center justify-content-center">
            <img src="img/IconsClaro/footer_logo.png" alt="logoClaro">
          </div>
          <div class="col-md-6 col-sm-12 d-flex align-items-center justify-content-center text-center">
            <p class="textFooterlegal">Aviso legal | política de privacidad | consideraciones generales del producto</p>
          </div>
          <div class="classMovilfootes2 col-md-3 col-sm-12 d-flex flex-column align-items-center justify-content-center text-center">
            <p class="textFooterinf">Infórmate llamando al</p>
            <span class="fontNumber">(01) 5102155</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row background-white">
    <div class="col-12 justify-content-center text-center">
      <p class="textFooter3">Este sitio web es propiedad de socios online perú, por lo que su contenido es exclusiva responsibilidad de dicha empresa</p>
    </div>
  </div>
</footer>

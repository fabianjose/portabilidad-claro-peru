<div class="ocultarForm">
	<div class="form-fixed-principal  col-lg-3 formClaro" id="form-principal">
		@component('components.form', ['ciudades' => $ciudades,'ambito' => 'form','color' => 'black'])
		@endcomponent

		<button class="btn-animate btn btn-block buttonFormPrimary">TE LLAMAMOS</button>
	</div>
</div>


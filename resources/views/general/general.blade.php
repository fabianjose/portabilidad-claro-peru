<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Portabilidad móvil Perú</title>

        <!-- Styles -->
        <!--<link rel="stylesheet" type="text/css" href="{{asset('librerias/bootstrap/css/bootstrap.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/bootstrap/css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/fontawesome/css/all.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/datatables/datatables.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/select2/dist/css/select2.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('css/general.css')}}"/>-->
        @for($i=0; $i < count($css); $i++)
          <!--   <link rel="stylesheet" type="text/css" href="{{$css[$i]}}"/> -->
        @endfor
    </head>
    <body>
        <div class="loader"></div>
        @yield('nav')
        @yield('header')
        @yield('content')
        @yield('footer')
    </body>

    <!-- Script -->
    <script type="text/javascript" async src="{{asset('librerias/jquery/jquery.min.js')}}"></script>


    <script type="text/javascript">
    var listaNegra = null;
    var horario_atencion = null;
    var lista_tsource = null;
    var loadScripts = null;

    var hideLoad = function() {
      $(".loader").css("display","none");
    }

    var functionsPostCallBack = function() {
      // script modulo pre-load
      var jspre= @json($jspre);
      for (var i = jspre.length - 1; i >= 0; i--) {
          var element = document.createElement("script");
          element.src = jspre[i];
          document.body.appendChild(element);

          //Coloco en el ultimo pre-script cargado un onload
          if (i == 0) { element.setAttribute('onload', 'loadScripts();'); }
      }
    }

    function downloadJSAtOnload() {

        loadScripts = function() {
          setTimeout(
            function () {

              // script general
              var element = document.createElement("script");
              element.src = "js/general.js";
              document.body.appendChild(element);

              // script modulo
              var js= @json($js);
              for (var i = js.length - 1; i >= 0; i--) {
                  var element = document.createElement("script");
                  element.src = js[i];
                  document.body.appendChild(element);

                  //Coloco en el ultimo script cargado un onload
                  if (i == 0) { element.setAttribute('onload', 'hideLoad();'); }
              }


            }
          , 0);
        };

        //setTimeout(loadScripts, 2500);

        // script bootstrap
        var element = document.createElement("script");
        element.src = "librerias/bootstrap/js/bootstrap.js";
        document.body.appendChild(element);
        // script bootstrap min
        var element = document.createElement("script");
        element.src = "librerias/bootstrap/js/bootstrap.min.js";
        document.body.appendChild(element);
        // script fontweasome
        var element = document.createElement("script");
        element.src = "librerias/fontawesome/js/all.min.js";
        document.body.appendChild(element);

        // script callback
        var element = document.createElement("script");
        element.src = "js/callback.js";
        element.setAttribute('onload', 'functionsPostCallBack();');
        document.body.appendChild(element);
    }

    if (window.addEventListener){
        window.addEventListener("load", function() {
          setTimeout(function() {
            downloadJSAtOnload();
          }, 0)
        }, false);
    }else if (window.attachEvent){
        window.attachEvent("onload", function() {
          setTimeout(function() {
            downloadJSAtOnload();
          }, 0)
        });
    }else{
        window.onload = function() { setTimeout(function() {  downloadJSAtOnload(); }, 0); }
    }

    function downloadCSSAtOnload() {
        var style = document.createElement("link");
        style.async = false;
        style.rel="stylesheet";
        style.type="text/css";
        style.href = 'librerias/bootstrap/css/bootstrap.css';
        document.head.appendChild(style);

        var style = document.createElement("link");
        style.async = false;
        style.rel="stylesheet";
        style.type="text/css";
        style.href = 'librerias/bootstrap/css/bootstrap.min.css';
        document.head.appendChild(style);

        var style = document.createElement("link");
        style.async = false;
        style.rel="stylesheet";
        style.type="text/css";
        style.href = 'librerias/fontawesome/css/all.min.css';
        document.head.appendChild(style);

        var style = document.createElement("link");
        style.async = false;
        style.rel="stylesheet";
        style.type="text/css";
        style.href = 'librerias/datatables/datatables.min.css';
        document.head.appendChild(style);

        var style = document.createElement("link");
        style.async = false;
        style.rel="stylesheet";
        style.type="text/css";
        style.href = 'librerias/select2/dist/css/select2.min.css';
        document.head.appendChild(style);

        var style = document.createElement("link");
        style.async = false;
        style.rel="stylesheet";
        style.type="text/css";
        style.href = 'css/general.css';
        document.head.appendChild(style);

        var css= @json($css);
        for (var i = css.length - 1; i >= 0; i--) {
            var style = document.createElement("link");
            style.async = false;
            style.rel="stylesheet";
            style.type="text/css";
            style.href = css[i];
            document.head.appendChild(style);
        }
    }

    if (window.addEventListener){
        window.addEventListener("load", function() {
          setTimeout(function() {
            downloadCSSAtOnload();
          }, 0)
        }, false);
    }else if (window.attachEvent){
        window.attachEvent("onload", function() {
          setTimeout(function() {
            downloadCSSAtOnload();
          }, 0)
        });
    }else{
        window.onload = function() { setTimeout(function() {  downloadCSSAtOnload(); }, 0); }
    }

</script>

    <!-- <script type="text/javascript" async src="{{asset('librerias/bootstrap/js/bootstrap.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/fontawesome/js/all.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('js/callback.js')}}"></script>
    <script type="text/javascript" async src="{{asset('js/general.js')}}"></script>-->
    @for($i=0; $i < count($js); $i++)
        <!--<script type="text/javascript" async src="{{$js[$i]}}"></script>-->
    @endfor
</html>

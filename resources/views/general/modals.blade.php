<!--<div class="modal fade registration-form-modal-general form-request-modal" tabindex="-1" role="dialog" id="mailModal">
	<div class="justify-center" style="color: #fff; width:100%;">
		<div class=" col-lg-3 formClaro " id="form-principal">
			@component('components.form', ['ciudades' => $ciudades,'ambito' => 'device','color' => 'white'])
			@endcomponent
			<button class="btn btn-block buttonFormPrimary">TE LLAMAMOS</button>
		</div>
	</div>
</div>
type="button" class="btn btn-info din-regular" data-toggle="modal" data-target="#mailModal"
type="button" class="btn btn-info din-regular" data-toggle="modal" data-target="#mailModal"-->

<!-- Modal -->
<div class="modal fade" id="mailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<div class="justify-center w100white w100white_modal" style="color:#fff;">
				<div class="formClaro formClaro_modal" id="form-principal">
          @component('components.form', ['ciudades' => $ciudades,'ambito' => 'device','color' => 'white','modal' => true,'btnClose' => true])
					@endcomponent
					<button class="btn btn-block buttonFormPrimary">TE LLAMAMOS</button>
				</div>
			</div>
    </div>
  </div>
</div>

<!-- Navigation -->
<nav class="navbar fixed-top navbar-light bg-light q-pa-0 barNavbar ">
  <div class="col-12 col-lg-4 col-md-3 LogoMovilcenter">
    <a class="navbar-brand" href="#">
      <img src="img/IconsClaro/logo_Claro.png" alt="">
    </a>
  </div>
  <div class="col-12 col-lg-8 col-md-8 BoxRed row justify-content-between align-items-center">
    <div class="col-2 q-pa-0 hidenCell">
      <div class="iconNav d-flex justify-content-between align-items-center">
        <img src="img/IconsClaro/phone.png" id="" class="imgPhone">
      </div>
    </div>
    <div class="col-6 col-md-5 textNavbar din-black">
      Lima: <a class="TeflNav" href="tel:015102155">(01) 5102155</a> <br>
      Arequipa: <a class="TeflNav" href="tel:054755103">(054) 755103</a>
    </div>
    <div class="col-6 col-md-5 textNavbar din-black">
      Cusco: <a class="TeflNav" href="tel:084488041">(084) 488041</a> <br>
      Trujillo: <a class="TeflNav" href="tel:044455043">(044) 455043</a>
    </div>
  </div>
</nav>

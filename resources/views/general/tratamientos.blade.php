<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Portabilidad móvil Perú</title>

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/bootstrap/css/bootstrap.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/bootstrap/css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/fontawesome/css/all.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/datatables/datatables.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('librerias/select2/dist/css/select2.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('css/general.css')}}"/>
    </head>
    <body>
        @include('general.nav2')
        @include('landing.home.contenido2')
        @include('general.footer')
    </body>
    <!-- Script -->
    <script type="text/javascript" async src="{{asset('librerias/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/bootstrap/js/bootstrap.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/fontawesome/js/all.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('librerias/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript" async src="{{asset('js/callback.js')}}"></script>
    <script type="text/javascript" async src="{{asset('js/general.js')}}"></script>
</html>

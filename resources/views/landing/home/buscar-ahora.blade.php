<div class="col-12 bg-white shadow text-style pr-0">	
	<!-- formulario  -->
	<div id="formulario-principal__" action="/reservar" method="GET" class="col-12 p-0 align-self-stretch">
		<form id="form-buscar_reserva" method="GET<" class="w-100 row m-0" action="/reserva">
		    <div class="col-lg-4 col-12 row m-0 text-center mb-2 mt-2 border-right2 pt-2 pb-2 align-items-center p-0">
		       <label class="col-12 text-center"><h5 class="text-bold">@lang('general.por_donde_quieres_navegar')</h5></label>
		    </div>
	    
		    <div class="col-lg-5 col-12 row m-0 text-center mb-2 pr-3 pl-3 mt-2">
		    	<div class="w-50 row m-0 border-right pr-3">
		    		<label class="col-12 text-center pt-2"><h5 class="text-bold">@lang('general.destino')</h5></label>
		    		<select id="destino" class="w-100 form-control" required="required">
		    			<option value="">@lang('general.placeholder_destino')</option>
		    		</select>
		    	</div>
		    	<div class="w-50 row m-0 pl-3">
		    		<label class="col-12 text-center pt-2"><h5 class="text-bold">@lang('general.fechas')</h5></label>
		    		<select id="fecha" class="w-100 form-control">
		    			<option value="">@lang('general.placeholder_fecha')</option>
		    		</select>
		    	</div> 
		    </div>

		    <div class="col-lg-3 col-12 row m-0 text-center mb-2 p-0">
		       <button type="submit" class="btn btn-primary text-style w-100 btn-color rounded-0" style="margin-bottom:-0.5rem;"><h5 class="text-bold">@lang('general.buscalo_ahora')</h5></button>
		    </div>
	    </form>
	</div>
</div>
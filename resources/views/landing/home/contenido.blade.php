<!-- Sections -->
<div class="background-section2">
  <h2 class="titleSection2">Conoce todos los <span class="ColorRed2">planes</span> que tenemos para <span class="ColorRed2">ti</span></h2>

  <div id="CarruselSeciton2" class="CarruselSeciton2 carousel slide WidthCarrusel" data-ride="carousel" >
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="row justify-content-center align-items-center">

          <div class="col-6 d-flex column justify-content-end q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-1.png" alt="PLAN 1">
          </div>
          <div class="col-6 d-flex column justify-content-start q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-3.png" alt="PLAN 3">
          </div>
          <div class="col-6 d-flex column justify-content-end q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-2.png" alt="PLAN 2">
          </div>
          <div class="col-6 d-flex column justify-content-start q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-4.png" alt="PLAN 4">
          </div>

        </div>
      </div>
      <div class="carousel-item">
        <div class="row justify-content-start">
          <div class="col-6 d-flex column justify-content-end q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-5.png" alt="PLAN 5">
          </div>
          <div class="col-6 d-flex column justify-content-start q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-7.png" alt="PLAN 7">
          </div>
          <div class="col-6 offget-6 d-flex column justify-content-end q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-6.png" alt="PLAN 6">
          </div>
        </div>
      </div>
    </div>
    <a class="ColorIndicator carousel-control-prev" href="#CarruselSeciton2" role="button" data-slide="prev">
      <span class="IconsControlSection carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="ColorIndicator carousel-control-next" href="#CarruselSeciton2" role="button" data-slide="next">
      <span class="IconsControlSection carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <div id="CarruselSeciton2Movil" class="CarruselSeciton2 carousel slide WidthCarrusel" data-ride="carousel" >
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="flex-column d-flex justify-content-center align-items-center">

          <div class="col-sm-9 col-10 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-1.png" alt="PLAN 1">
          </div>
          <div class="col-sm-9 col-10 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-2.png" alt="PLAN 2">
          </div>

        </div>
      </div>
      <div class="carousel-item">
        <div class="flex-column d-flex justify-content-center align-items-center">

          <div class="col-sm-9 col-10 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-3.png" alt="PLAN 3">
          </div>
          <div class="col-sm-9 col-10 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-4.png" alt="PLAN 4">
          </div>

        </div>
      </div>
      <div class="carousel-item">
        <div class="flex-column d-flex justify-content-center align-items-center">
          <div class="col-sm-9 col-10 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-5.png" alt="PLAN 5">
          </div>
          <div class="col-sm-9 col-10 offget-6 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-6.png" alt="PLAN 6">
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="flex-column d-flex justify-content-center align-items-center">
          <div class="col-sm-9 col-10 d-flex column justify-content-center q-pb-20 q-pt-20">
            <img class="ClassImgModalOpen" data-toggle="modal" data-target="#mailModal" src="img/Carrusel-planes/Planes-MAX-Ilimitado-7.png" alt="PLAN 7">
          </div>
        </div>
      </div>
    </div>
    <a class="ColorIndicator carousel-control-prev" href="#CarruselSeciton2Movil" role="button" data-slide="prev">
      <span class="IconsControlSection carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="ColorIndicator carousel-control-next" href="#CarruselSeciton2Movil" role="button" data-slide="next">
      <span class="IconsControlSection carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="btnSeciton2 d-flex justify-content-center align-items-center">
    <button type="button" class="btn btn-dark btnColor2" data-toggle="modal" data-target="#mailModal">TE LLAMAMOS</button>
  </div>
</div>

<div class="background-section3">
  <div class="fhaterBox d-flex justify-content-center align-items-center">
    <div class="childBox text-center">
      <p class="textporque">Por qué conformate con un plan nacional si puedes tener</p>
      <img class="movilImageSection3" src="img/IconsClaro/max-18.svg" alt="MAX">
    </div>
  </div>
</div>

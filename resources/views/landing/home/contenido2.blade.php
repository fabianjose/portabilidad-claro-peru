<!-- Sections -->
<div class="father-box">
  <h1 class="title text-center"> Ley de Protección de Datos Personales</h1>
  <p>SOCIOS ON LINE S.A.S. SUCURSAL DEL PERU y BACKUP SERVICE PERU SAC, es una sociedad interesada en garantizar,
    respetar y cumplir de manera estricta el derecho fundamental a la protección de los datos personales,
    previsto en el Artículo 2° numeral 6 de la Constitución Política del Perú.</p>
    <p>En tal sentido, le comunicamos que realizamos el tratamiento de los datos personales, utilizando estándares
      de alta calidad, ello con la finalidad de mantener la confidencialidad de los mismos, cumpliendo lo ordenado
      en la Ley N° 29733 - Ley de Protección de Datos Personales (en adelante la Ley), su Reglamento aprobado por
      el Decreto Supremo N° 003-2013-JUS y las demás normas que derogue y/o modifique las normas existentes.</p>
      <p>Como es nuestra intención, mantener una relación duradera, es indispensable que usted nos otorgue su
        consentimiento para tratar sus datos personales, para ello es necesario brindarle la siguiente información:
      </p>
      <p><span>1. ¿Qué son los datos personales?</span><br> Conforme lo define la ley; los datos personales es
        toda información sobre una persona natural que la identifica o la hace identificable a través de medios que
        pueden ser razonablemente utilizados. tenemos por ejemplo los datos de carácter identificativo, como lo son
        nombres, apellidos, n° DNI, n° ruc, n° de transporte, dirección del domicilio, teléfono, dirección de correo
        electrónico, imagen, voz, firma, firma electrónica; datos de características personales; datos económicos
        -financieros y de seguros, entre otros. Pudiendo acceder de igual forma a sus datos personales, en forma
        física, oral o electrónica, sea a través de fuentes públicas o de terceros.</p>
        <p><span>2. ¿Es obligatorio obtener autorización para el tratamiento de sus datos personales?</span><br> Sí,
          los datos personales sólo pueden ser objeto de tratamiento con consentimiento del titular, salvo que la ley
          lo autorice en determinada situación. De igual manera es preciso indicar que el consentimiento debe ser
          libre, previo, informado, expreso e inequívoco.</p>
          <p><span>3. ¿SOCIOS ON LINE S.A.S SUCURSAL DEL PERU y BACKUP SERVICE PERU SAC es el titular del Banco de Datos
            donde se incluirán sus datos personales?</span><br>
          </p><p>Si, al aceptar nuestra política de privacidad, sus datos personales brindados son tratados por la
            empresa. Por ello, es importante precisar que al haber dado usted su consentimiento para el tratamiento
            de sus datos personales, nuestra empresa está autorizada a almacenar, procesar y transferir sus datos
            personales a otras empresas vinculadas.</p>
            <p>Si, al aceptar nuestra política de privacidad, sus datos personales brindados son tratados por la
              empresa. Por ello, es importante precisar que al haber dado usted su consentimiento para el tratamiento
              de sus datos personales, nuestra empresa está autorizada a almacenar, procesar y transferir sus datos
              personales a otras empresas vinculadas.</p>
              <p>Los datos personales serán incluidos en nuestro Banco de Datos Clientes y nuestra finalidad es informar a
                nuestros clientes las actualizaciones de nuestros servicios y/o productos.</p>
                <p></p>
                <p><span>4. ¿Ud. autoriza el ofrecimiento de nuevos productos y/o servicios que oferte la
                  empresa?</span><br> Si usted acepta la presente política de privacidad, SOCIOS ON LINE S.A.S SUCURSAL
                  DEL PERU y BACKUP SERVICE PERU SAC, se comunicará con usted y será informado a través de medios telemáticos,
                  llamadas, sistemas de llamadas telefónicas, mensajes de texto u otros mensajes electrónicos de uso masivo de
                  comunicación de nuevos productos y servicios, que sean promovidos como resultado de campañas de promoción y
                  marketing que realice la empresa.</p>
                  <p><span>5. ¿Es factible que usted revoque el consentimiento para el tratamiento de sus datos
                    personales?</span><br> Sí, usted puede revocar el consentimiento brindado en cualquier momento, sin
                    justificación previa. Se puede revocar el consentimiento para las finalidades autorizadas, ya sea de manera
                    parcial o total.<!--9-->
                  </p><p><span>6. ¿Qué derechos ejercen los titulares de los datos personales?</span><br>
                  </p><p>Usted está facultado hacer uso de los llamados Derechos ARCO; (i) Derecho de Acceso, solicitando
                    información sobre los datos personales registrados en el banco de datos; (ii) Derecho de
                    Rectificación (actualización inclusión) puedes modificar los datos brindados, ya sea porque exista
                    algún error, sean falsos, incompletos u otra causa; (iii) Derecho de Cancelación (Supresión)
                    ejerciendo este derecho, es posible solicitar la supresión o cancelación de sus datos personales de
                    nuestro banco de datos, sin justificación previa. Precisamos que, en el caso, exista una relación
                    comercial con la empresa, mantendremos los datos considerando que son esenciales para la ejecución
                    del mismo y (iv) Derecho de Oposición, toda persona tienen la facultad de oponerse al uso de sus
                    datos personales, cuando estos no han sido autorizados, asimismo puede oponerse por una situación
                    personal concreta.</p>
                    <p>Para el ejercicio de sus derechos ARCO, puede dirigirse a nuestras oficinas, ubicada en la calle
                      Juana Alarcón de Dammert 278, Distrito Miraflores en la Provincia de Lima y Departamento de Lima. es
                      importante que te acerques con tu DNI, con la finalidad de poder identificarte como titular de los
                      datos personales, o con tu poder en caso de representación o el registro correspondiente que
                      acredite la representación.</p>
                      <p></p>
</div>

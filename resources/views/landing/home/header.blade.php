<!-- Header -->
<!-- <div class="movilDisplay justify-content-center">
  <img class="imgMovilMB" src="img/banner_fondo_peru_movil.jpg" alt="MB">
</div> -->
<header class="background-header row-flex align-items-end">
  <div class="headerBoxCarrousel">
    <div class="d-column carrouselMain">
      <div class="owl-carousel owl-theme" >
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Samsung A30</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/Samsung_A30.png" alt="A30">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Samsung A50</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/Samsung_A50.png" alt="A50">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Samsung A20</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/Samsung_A20.png" alt="A20">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Samsung A10</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/Samsung_A20.png" alt="A20">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Huawey P30</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/huawey_p30_lite.png" alt="P30">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Huawey psmart</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/Huawey_psmart_2019.png" alt="2019">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Iphone XR</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/iphone_XR.png" alt="XR">
            </div>
          </div>
        </div>
        <div class="owl-custom-item">
          <div class="d-flex CarruselHeaderCenter d-column justify-content-center align-items-center">
            <div class="cHeader">
              <h4 align="center" class="textCellHeader">Xiaomi redmi note 7</h4>
            </div>
            <div class="d-flex justify-content-center">
              <img class="imgCarruselPhone" src="img/Carrusel-telf/Xiaomi_redmi_note_7.png" alt="note7">
            </div>
          </div>
        </div>
      </div>
      <div class="ConoceMas row-flex justify-center">
        <button type="button" class="btn btn-info din-regular" data-toggle="modal" data-target="#mailModal">Conoce más</button>
      </div>

    </div>
  </div>
</header>
 @include('general.form-principal')

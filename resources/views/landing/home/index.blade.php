@extends('general.general')

@section('nav')
    @include('general.nav')
@endsection

@section('header')
    @include('landing.'.$modulo.'.header')
@endsection

@section('content')
    @include('landing.'.$modulo.'.contenido')
    @include('general.modals')
@endsection

@section('content1')
    @include('landing.home.contenido2')
@endsection

@section('footer')
    @include('general.footer')
@endsection
